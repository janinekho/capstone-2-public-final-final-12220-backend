const jwt = require('jsonwebtoken');
const secret = 'CourseBookingAPI';

module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {})
}

//passes sa req to either a callback fucntion or sa middleware..
module.exports.verify = (req, res, next) => {
//token na kinukuha sa header. we are now saving this in this variable. verify if the access token is legitimate
	let token = req.headers.authorization;// we are grabbing the token in our headers

	console.log(token);

	if(typeof token !=='undefined'){
		//'Bearer' eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmYjdiN2ZkMDQ0ZjVmZGUyMGI0MmM4YiIsImVtYWlsIjoicGV0ZWtAZ21haWwuY29tIiwiaXNBZG1pbiI6ZmFsc2UsImlhdCI6MTYwNTg3NzE5NH0.qIsBiGduNA2qUlhLAZkNXB5pMAbsq9eGTkS35-qVwl
		token = token.slice(7, token.length);

		console.log(token);

		return jwt.verify(token,secret,(err,data) => {
			//next()passes the request to next callback function/middleware
			return(err)? res.send({auth:'failed'}) : next() // valid and correct next() will run.
		})

	//sayHello(param1, sayBye())
	//sayBye is a callback function
	} else{
		return res.send({auth:'failed'})
	}
}
// asdfweiu1234 = hello123
// 128j91j2e8j1 = hello123

module.exports.decode = (token) => {
	if(typeof token !== 'undefined'){
		token = token.slice(7, token.length)// slicing off the word na bearer para yung code kagad
	
	return jwt.verify(token, secret, (err,data) => {
	//complete true - grab anything that's under the request tag and the payload ( information na pinapasa- id, email, and isAdmin)

		return(err) ? null : jwt.decode(token, {complete:true}).payload
	})
    } else {
	   return null
    }

}




