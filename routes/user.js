const express = require('express')
const router = express.Router()
const UserController = require('../controllers/user')
const auth = require('../auth')

//Check if email exists
router.post('/email-exists', (req, res) => {
	UserController.emailExists(req.body).then(resultFromEmailExists => res.send(resultFromEmailExists))
})

//User Registration
router.post('/', (req, res) => {
	UserController.register(req.body).then(resultFromRegister => res.send(resultFromRegister))
})


//Login
router.post('/login', (req, res) => {
	UserController.login(req.body).then(resultFromLogin => res.send(resultFromLogin))
})

//RETRIEVE USER DETAILS
//auth.verify-middleware- later on we will be creating a function. why? it will actually try to do something. usually for verification. 
router.get('/details', auth.verify,(req, res)=>{
	//aut.decode- function
	//user ={
	/*	id: user._id,
		email:
		isAdmin:
	}*/
	const user = auth.decode(req.headers.authorization)//getting the info from the head tag// pasa ang user id to.get and retrieve the details lang.
	//.get function ang pinapasa ay yung id which is id: user._id
	UserController.get({userId: user.id}).then(resultFromDetails => res.send(resultFromDetails))

})

//Retrieve By User Id
router.get('/:userId', (req, res) => {
	const userId = req.params.userId
	//it has not key value kasi declared na sa variable itself. dito object
    UserController.get({ userId }).then(user => res.send(user)) 
})





//Enroll a user
//auth.verify- legimate user
router.post('/enroll', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
   //params - userId and course
	UserController.enroll(params).then(result => res.send(result))
}) 

//updaing user

router.put('/', auth.verify, (req, res) => {
	
	UserController.updateUser(req.body).then(result => res.send(result))
   
}) 



module.exports = router